import fastify from 'fastify'
import mercurius from "mercurius";

const server = fastify()


const schema=`
  type Query{
    add( x:Int, y:Int): Int
  }
  
`

const resolvers={
  Query:{
    add: async(_, {x,y})=> {
      console.debug(x, y, typeof x, typeof y)
      return x + y;
    }
  }
}

server.register(mercurius,{
  schema,
  resolvers,
  graphiql:true
})

server.get('/', async (request, reply) => {
  const q='{ add(x:2, y:19 ) }';
  console.debug(q)
  return reply.graphql(q);
})

server.listen({ port: 5000,  host: "0.0.0.0" }, (err, address) => {
  if (err) {
    console.error(err)
    process.exit(1)
  }
  console.log(`Server listening at ${address}`)
})

